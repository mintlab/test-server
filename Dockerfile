FROM node:latest

# Create app directory
RUN mkdir -p /opt/api
WORKDIR /opt/api

# Install app dependencies
COPY package.json /opt/api
RUN npm install

COPY server.js /opt/api

CMD [ "npm", "start" ]