# Setup NTN Api

* Build

```
docker build -t registry.gitlab.com/mintlab/test-server .
```

* Run

```
docker run -p 8080:80 registry.gitlab.com/mintlab/test-server
```

* Publish

```
docker push registry.gitlab.com/mintlab/test-server
```